﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class VentaLibro
    {
        public int Id { get; set; }
        public int VentaId { get; set; }
        public int LibroId { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
    }
}
