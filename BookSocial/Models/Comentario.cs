﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        [Required(ErrorMessage = "El campo detalle es obligatorio")]
        [MinLength(5, ErrorMessage ="Detalle debe tener al menos 5 caracateres")]
        public string Detalle { get; set; }
    }
}
