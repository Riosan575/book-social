﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }        
        public string Caratula { get; set; }
        [Required]
        public int AutorId { get; set; }
        public bool EstaActivo { get; set; }
        [Required]
        public DateTime FechaPublicacion { get; set; }
        public List<LibroAutor> LibroAutores { get; set; }
        public List<BookLike> Likes { get; set; }
    }
}
