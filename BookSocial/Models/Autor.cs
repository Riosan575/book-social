﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial.Models
{
    public class Autor
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        //public List<Book> Books { get; set; }
    }
}
