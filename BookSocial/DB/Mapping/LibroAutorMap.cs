﻿using BookSocial.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial.DB.Mapping
{
    public class LibroAutorMap : IEntityTypeConfiguration<LibroAutor>
    {
        public void Configure(EntityTypeBuilder<LibroAutor> builder)
        {
            builder.ToTable("LibroAutor");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Autor)
                .WithMany()
                .HasForeignKey(o => o.AutorId);
        }
    }
}
