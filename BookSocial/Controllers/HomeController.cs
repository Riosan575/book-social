﻿using BookSocial.DB;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookSocial.Controllers
{
    public class Ciudad { 
        public string Pais { get; set; }
        public string Nombre { get; set; }
    }

    public class HomeController : Controller
    {

        public List<string> Paises = new List<string>() { "Perú", "Chile", "Argentina" };
        public List<Ciudad> Ciudades = new List<Ciudad>() { 
            new Ciudad { Pais = "Perú", Nombre = "Lima"},
            new Ciudad { Pais = "Perú", Nombre = "Cajamarca"},
            new Ciudad { Pais = "Chile", Nombre = "Santigado de Chile"},
            new Ciudad { Pais = "Chile", Nombre = "Viña del Mar"},
            new Ciudad { Pais = "Argentina", Nombre = "Buenos Aires"},
            new Ciudad { Pais = "Argentina", Nombre = "Córdoba"}
        };

        private AppBookSocialContext context;
        public HomeController(AppBookSocialContext context)
        {
            this.context = context;
        }

        public IActionResult Combo()
        {
            ViewBag.Paises = Paises;
            return View();
        }


        public List<Ciudad> GetCiudadesJson(string pais)
        {
            return Ciudades.Where(o => o.Pais == pais).ToList();
        }


        public ViewResult GetCiudadesHtml(string pais)
        {
            var ciudades = Ciudades.Where(o => o.Pais == pais).ToList(); 
            return View("Ciudades", ciudades);
        }

        public ViewResult GetPaisesHtml()
        {            
            return View("Paises", Paises);
        }

        public IActionResult Index()
        {
            //throw new Exception();
            var books = context.Books
                .Include("LibroAutores.Autor")
                .Include("Likes")
                .ToList();
            return View("Index", books);
        }
    }
}
