﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial.Models;
using Microsoft.AspNetCore.Mvc;

namespace BookSocial.Controllers
{
    public class VentaController : Controller
    {
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Create(Venta venta, List<VentaLibro> libros)
        {
            // Guarde antes venta

            foreach(var item in libros)
            {
                item.VentaId = venta.Id;
            }

            /**
             * 1. Guardo venta
             * 2. Actualizo idVenta en Ventalibros
             * 3. guardo ventaLibros
             * 
             * **/
            return RedirectToAction("Index");
        }
    }
}
